﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace SchedulerLib
{
    [DataContract]
    public class ScheduledExecution
    {
        public ScheduledExecution()
        {
            Id = Guid.NewGuid();
        }

        [DataMember]
        public Guid Id { get; private set; }

        [DataMember]
        public DateTime At { get; set; }

        [DataMember]
        public string Command { get; set; }

        [DataMember]
        public string Client { get; set; }
    }
}
