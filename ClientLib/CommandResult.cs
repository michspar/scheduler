﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ClientLib
{
    [DataContract]
    public class CommandResult
    {
        [DataMember]
        public string Output { get; set; }

        [DataMember]
        public string Error { get; set; }

        [DataMember]
        public int ExitCode { get; set; }
    }
}
