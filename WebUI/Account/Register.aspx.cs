﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using WebUI.Models;

namespace WebUI.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = new ApplicationUser() { UserName = Login.Text };
            var result = manager.Create(user, Password.Text);

            if (result.Succeeded && manager.AddToRole(user.Id, "netops").Succeeded )
            {
                IdentityHelper.RedirectToReturnUrl("~/Account/Manage.aspx?u=" + Login.Text, Response);
            }
            else 
            {
                ErrorMessage.Text = result.Errors.FirstOrDefault();
            }
        }
    }
}