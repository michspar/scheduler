﻿using SchedulerLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;

namespace WebUI
{
    public partial class _Default : Page
    {
        CommandRunContext db;

        protected void Page_Load(object sender, EventArgs e)
        {
            db = Context.GetOwinContext().GetUserManager<CommandRunContext>();
        }

        protected void Delete_Click(object sender, EventArgs e)
        {
            var clientName = (sender as LinkButton).CommandArgument;

            if (string.IsNullOrWhiteSpace(clientName))
                return;

            var client = db.Clients.SingleOrDefault(x => x.Name == clientName);

            if (client == null)
                return;

            db.Clients.Remove(client);
            db.SaveChanges();
            Response.Redirect(Request.Url.AbsolutePath);
        }

        protected void createBtn_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(newClientText.Text))
                return;

            db.GetOrAddClient(newClientText.Text);
            db.SaveChanges();
            Response.Redirect(Request.Url.AbsolutePath);
        }
    }
}