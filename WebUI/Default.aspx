﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebUI._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:LoginView runat="server">
        <RoleGroups>
            <asp:RoleGroup Roles="admins">
                <ContentTemplate>
                    <div class="jumbotron">
                        <h1>Manage accounts</h1>
                        <p class="lead">Add or remove users from admins and/or networkOperators groups</p>
                        <p>
                            <asp:HyperLink runat="server" NavigateUrl="~/Account/ManageAccounts.aspx" class="btn btn-primary btn-lg">Open &raquo;</asp:HyperLink>
                        </p>
                    </div>
                </ContentTemplate>
            </asp:RoleGroup>
        </RoleGroups>
    </asp:LoginView>

    <div class="form-inline row">
        <div class="col-md-4">
            <h2>Manage clients</h2>
            <p>
                <asp:DataList runat="server" DataSourceID="LinqClientsDataSource">
                    <ItemTemplate>
                        <asp:HyperLink ID="NameLabel" runat="server" Text='<%# Eval("Name") %>' NavigateUrl='<%# "~/ManageClient.aspx?c=" + Eval("Name") %>' CssClass="form-control-static" />
                        &nbsp;
                    <asp:LinkButton runat="server" Text="Delete" OnClick="Delete_Click" CommandArgument='<%# Eval("Name") %>' CssClass="btn btn-default" />
                    </ItemTemplate>
                </asp:DataList>
            </p>
            <p>
                <asp:TextBox runat="server" ID="newClientText" CssClass="form-control" /><asp:Button runat="server" Text="Create" CssClass="btn btn-default" ID="createBtn" OnClick="createBtn_Click" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="newClientText" CssClass="text-danger" ErrorMessage="The client field can't be empty." />
                <asp:LinqDataSource ID="LinqClientsDataSource" runat="server" EntityTypeName="" ContextTypeName="SchedulerLib.Data.CommandRunContext" Select="new (Name)" TableName="Clients" />
            </p>
        </div>
    </div>

</asp:Content>
