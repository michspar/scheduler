﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Owin;
using WebUI.Models;

namespace WebUI.Account
{
    public partial class Manage : System.Web.UI.Page
    {
        protected string SuccessMessage
        {
            get;
            private set;
        }

        private bool HasPassword()
        {
            return manager.HasPassword(GetUserId());
        }

        public string GetUserId()
        {
            var userName = Request.QueryString["u"];

            if (ManageSelf)
                return User.Identity.GetUserId();

            return manager.Users.Where(u => string.Compare(u.UserName, userName, true) == 0).Select(u => u.Id).SingleOrDefault() ?? User.Identity.GetUserId();
        }

        public bool ManageSelf
        {
            get
            {
                return Request.QueryString["u"] == null || !User.IsInRole("admins");
            }
        }

        ApplicationUserManager manager;

        protected void Page_Load()
        {
            manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationRoleListProvider.Manager = Context.GetOwinContext().GetUserManager<ApplicationRoleManager>();

            notForAdminSection.Visible = manager.FindById(GetUserId()).UserName != "admin";

            UserRole.Enabled = User.IsInRole("admins");

            if (!IsPostBack)
            {
                // Determine the sections to render
                if (HasPassword())
                {
                    ChangePassword.Visible = true;
                }
                else
                {
                    CreatePassword.Visible = true;
                    ChangePassword.Visible = false;
                }

                // Render success message
                var message = Request.QueryString["m"];
                if (message != null)
                {
                    // Strip the query string from action
                    Form.Action = ResolveUrl("~/Account/Manage");

                    SuccessMessage =
                        message == "ChangePwdSuccess" ? "Your password has been changed."
                        : message == "SetPwdSuccess" ? "Your password has been set."
                        : message == "RemoveLoginSuccess" ? "The account was removed."
                        : message == "AddPhoneNumberSuccess" ? "Phone number has been added"
                        : message == "RemovePhoneNumberSuccess" ? "Phone number was removed"
                        : String.Empty;
                    successMessage.Visible = !String.IsNullOrEmpty(SuccessMessage);
                }
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        protected void DeleteUser_Click(object sender, EventArgs e)
        {
            if (GetUserId() == User.Identity.GetUserId())
                Context.GetOwinContext().Authentication.SignOut();

            manager.DeleteAsync(manager.FindById(GetUserId()));

            IdentityHelper.RedirectToReturnUrl("~/Account/ManageAccounts.aspx", Response);
        }

        protected void UserRole_DataBound(object sender, EventArgs e)
        {
            UserRole.Items.FindByText(manager.GetRoles(GetUserId()).Single()).Selected = true;
        }

        protected void UserRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!User.IsInRole("admins"))
                return;

            var userId = GetUserId();

            manager.RemoveFromRoles(userId, manager.GetRoles(userId).ToArray());
            manager.AddToRole(userId, UserRole.SelectedValue);
        }
    }
}