﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SchedulerLib.Data
{
    public class BaseEntity
    {
        public int Id { get; set; }
    }
}