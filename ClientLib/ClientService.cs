﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace ClientLib
{
    public class ClientService : ICommandRunner
    {
        public CommandResult RunCommand(string commandLine)
        {
            commandLine = commandLine.Trim();

            var iCmd = (commandLine.StartsWith("\"") ? commandLine.IndexOf('"', 1) : commandLine.IndexOf(' ')) + 1;
            var module = iCmd > 0 ? commandLine.Substring(0, iCmd) : commandLine;
            var args = iCmd > 0 ? commandLine.Substring(iCmd) : null;
            var p = new Process()
            {
                EnableRaisingEvents = false,
                StartInfo = new ProcessStartInfo(module, args)
                {
                    CreateNoWindow = false,
                    ErrorDialog = false,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    WindowStyle = ProcessWindowStyle.Normal,
                    UseShellExecute = false
                }
            };

            var output = new MemoryStream();

            try
            {
                p.Start();
                p.StandardOutput.BaseStream.CopyTo(output);

                while (!p.WaitForExit(1000))
                    p.StandardOutput.BaseStream.CopyTo(output);

                output.Seek(0, SeekOrigin.Begin);

                return new CommandResult()
                {
                    Output = new StreamReader(output, Console.InputEncoding).ReadToEnd(),
                    Error = p.StandardError.ReadToEnd(),
                    ExitCode = p.ExitCode
                };
            }
            catch (Exception ex)
            {
                return new CommandResult()
                {
                    Error = ex.Message,
                    ExitCode = -1
                };
            }
        }
    }
}
