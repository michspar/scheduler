﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;
using WebUI.Models;

namespace WebUI.Account
{
    public partial class ManageAccounts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ApplicationUsersListProvider.Manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            ApplicationRoleListProvider.Manager = Context.GetOwinContext().GetUserManager<ApplicationRoleManager>();
        }
    }
}