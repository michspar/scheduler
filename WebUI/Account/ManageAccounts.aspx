﻿<%@ Page Title="Manage Accounts" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageAccounts.aspx.cs" Inherits="WebUI.Account.ManageAccounts" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <asp:DataList runat="server" ID="Accounts" DataSourceID="LinqUsersDataSource">
        <ItemTemplate>
            <p>
                <asp:HyperLink ID="UserNameLabel" runat="server" Text='<%# Eval("UserName") %>' NavigateUrl='<%# "~/Account/Manage.aspx?u=" + Eval("UserName")  %>' />
            </p>
        </ItemTemplate>
    </asp:DataList>
    <asp:LinqDataSource ID="LinqUsersDataSource" runat="server" ContextTypeName="WebUI.ApplicationUsersListProvider" EntityTypeName="" Select="new (UserName)" TableName="Users" />
    <p><a href="Register.aspx" class="btn btn-primary btn-lg">Create user</a></p>
</asp:Content>
