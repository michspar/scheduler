﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLib.Data
{
    public class ClientCommandRun : BaseEntity
    {
        public DateTime ScheduleDate { get; set; }

        public virtual Client Client { get; set; }
        public virtual Command Command { get; set; }
        public virtual ClientLog Log { get; set; }
    }
}
