﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLib.Data
{
    public class ClientLog : BaseEntity
    {
        public string Output { get; set; }
        public string Error { get; set; }
        public int ExitCode { get; set; }

        public virtual List<ClientCommandRun> Runs { get; set; }
    }
}
