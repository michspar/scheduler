﻿<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Manage.aspx.cs" Inherits="WebUI.Account.Manage" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>

    <div>
        <asp:PlaceHolder runat="server" ID="successMessage" Visible="false" ViewStateMode="Disabled">
            <p class="text-success"><%: SuccessMessage %></p>
        </asp:PlaceHolder>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-horizontal">
                <h4>Change <%=ManageSelf ? "your" : Request.QueryString["u"] %> account settings</h4>
                <hr />
                <dl class="dl-horizontal">
                    <dt>Password:</dt>
                    <dd>
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Change]" Visible="false" ID="ChangePassword" runat="server" />
                        <asp:HyperLink NavigateUrl="/Account/ManagePassword" Text="[Create]" Visible="false" ID="CreatePassword" runat="server" />
                    </dd>
                    <asp:PlaceHolder ID="notForAdminSection" runat="server">
                        <dt>Role:</dt>
                        <dd>
                            <asp:DropDownList Width="100" runat="server" ID="UserRole" DataSourceID="LinqRolesDataSource" OnDataBound="UserRole_DataBound" DataValueField="Name" DataTextField="Name" OnSelectedIndexChanged="UserRole_SelectedIndexChanged" AutoPostBack="true" CssClass="form-control" />
                            <asp:LinqDataSource ID="LinqRolesDataSource" runat="server" ContextTypeName="WebUI.ApplicationRoleListProvider" EntityTypeName="" TableName="Roles" />
                        </dd>
                        <dd>&nbsp;</dd>
                        <dd>
                            <asp:Button runat="server" Text="Delete user" CssClass="btn btn-default" ID="DeleteUser" OnClick="DeleteUser_Click" /></dd>
                    </asp:PlaceHolder>
                </dl>
            </div>
        </div>
        <asp:LoginView runat="server">
            <RoleGroups>
                <asp:RoleGroup Roles="admins">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <a href="ManageAccounts.aspx" class="btn btn-primary btn-lg">Manage another account</a>
                        </div>
                    </ContentTemplate>
                </asp:RoleGroup>
            </RoleGroups>
        </asp:LoginView>
    </div>

</asp:Content>
