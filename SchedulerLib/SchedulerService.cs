﻿using SchedulerLib.ClientServiceReference;
using SchedulerLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace SchedulerLib
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class SchedulerService : ISchedulerService
    {
        LinkedList<Tuple<Task, ScheduledExecution>> scheduleList = new LinkedList<Tuple<Task, ScheduledExecution>>();

        public void ScheduleExecution(string workstation, string commandLine, DateTime at)
        {
            LinkedListNode<Tuple<Task, ScheduledExecution>> taskNode = null;

            taskNode = scheduleList.AddLast(new Tuple<Task, ScheduledExecution>(
                new Task(() =>
                {
                    scheduleList.Remove(taskNode);
                    RunExecution(taskNode.Value.Item2.Client, taskNode.Value.Item2.Command);
                }), new ScheduledExecution()
                {
                    At = at,
                    Client = workstation,
                    Command = commandLine
                }
                ));
        }

        Timer timer = new Timer(TimeSpan.FromSeconds(1).TotalMilliseconds);

        public SchedulerService()
        {
            timer.AutoReset = true;
            timer.Elapsed += (o, e) => scheduleList.Where(i => (i.Item2.At - DateTime.Now).TotalSeconds < 1 && i.Item1.Status == TaskStatus.Created).ToList().ForEach(i => i.Item1.Start());
            timer.Start();
        }

        public void RunExecution(Guid Id)
        {
            var scheduledItem = scheduleList.FirstOrDefault(i => i.Item2.Id == Id);

            if (scheduledItem == null)
                return;

            scheduledItem.Item1.Start();
        }

        void RunExecution(string workstation, string commandLine)
        {
            using (var db = new CommandRunContext())
            {
                var client = db.GetOrAddClient(workstation);
                var command = db.GetOrAddCommand(commandLine);
                var commandRun = db.AddCommandRun(client, command, DateTime.Now);
                var log = commandRun.Log;
                CommandRunnerClient clientConnection = null;

                try
                {
                    clientConnection = new CommandRunnerClient("BasicHttpBinding_ICommandRunner", string.Format(addressMask, workstation));

                    var res = clientConnection.RunCommand(commandLine);

                    log.Error = res.Error;
                    log.ExitCode = res.ExitCode;
                    log.Output = res.Output;
                }
                catch (Exception ex)
                {
                    log.Error = ex.Message;
                    log.ExitCode = -1;

                    ex = ex.InnerException;

                    while (ex != null)
                    {
                        log.Error += Environment.NewLine + ex.Message;
                        ex = ex.InnerException;
                    }
                }
                finally
                {
                    clientConnection.Close();
                }

                db.SaveChanges();
            }
        }

        static string addressMask = "http://{0}:8733/Design_Time_Addresses/ClientLib/ClientService/";


        public IEnumerable<ScheduledExecution> GetSchedule()
        {
            return scheduleList.Select(i => i.Item2);
        }


        public void CancelSchedule(Guid id)
        {
            var item = scheduleList.FirstOrDefault( i=> i.Item2.Id == id);

            if (item == null)
                return;

            scheduleList.Remove(item);
        }
    }
}
