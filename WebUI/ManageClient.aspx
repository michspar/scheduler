﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ManageClient.aspx.cs" Inherits="WebUI.ManageClient" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>Scheduled</h3>
    <div class="form-inline">
        <asp:DataList runat="server" ID="ScheduledList">
            <HeaderTemplate>
                <asp:Label Text="Command" runat="server" CssClass="form-control-static" Width="200" />
                <asp:Label Text="Schedule date" runat="server" CssClass="form-control-static" Width="150" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:TextBox runat="server" Text='<%# Eval("Command") %>' ReadOnly="true" CssClass="form-control" Width="200" />
                <asp:TextBox runat="server" Text='<%# Eval("At") %>' TextMode="DateTime" ReadOnly="true" CssClass="form-control" Width="150" />
                <asp:LinkButton runat="server" Text="Run" OnClick="Exec_Click" CommandArgument='<%# Eval("Id") %>' CssClass="form-control" Width="50" />
                <asp:LinkButton runat="server" Text="Delete" OnClick="DeleteSchedule_Click" CommandArgument='<%# Eval("Id") %>' CssClass="form-control" Width="65" />
            </ItemTemplate>
        </asp:DataList>
        <hr />
        <h3>Completed</h3>
        <asp:DataList runat="server" DataSourceID="LinqClientRunsDataSource">
            <HeaderTemplate>
                <asp:Label Text="Command" runat="server" CssClass="form-control-static" Width="200" />
                <asp:Label Text="Schedule date" runat="server" CssClass="form-control-static" Width="150" />
                <asp:Label Text="Exit code" runat="server" CssClass="form-control-static" Width="50" />
                <asp:Label Text="Output" runat="server" CssClass="form-control-static" Width="350px" />
                <asp:Label Text="Erorrs" runat="server" CssClass="form-control-static" Width="350px" />
            </HeaderTemplate>
            <ItemTemplate>
                <asp:TextBox runat="server" Text='<%# Eval("Command.AppWithArgs") %>' ReadOnly="true" CssClass="form-control" Width="200" />
                <asp:TextBox runat="server" Text='<%# Eval("ScheduleDate") %>' TextMode="DateTime" ReadOnly="true" CssClass="form-control" Width="150" />
                <asp:TextBox runat="server" Text='<%# Eval("Log.ExitCode") %>' TextMode="Number" ReadOnly="true" CssClass="form-control" Width="50" />
                <asp:TextBox runat="server" Text='<%# Eval("Log.Output") %>' Height="200px" Width="350px" TextMode="MultiLine" Wrap="false" ReadOnly="true" CssClass="form-control" />
                <asp:TextBox runat="server" Text='<%# Eval("Log.Error") %>' Height="200px" Width="350px" TextMode="MultiLine" Wrap="false" ReadOnly="true" CssClass="form-control" />
            </ItemTemplate>
        </asp:DataList>
        <asp:LinqDataSource ID="LinqClientRunsDataSource" runat="server" ContextTypeName="SchedulerLib.Data.CommandRunContext" EntityTypeName="" Select="new (ScheduleDate, Command, Log)" TableName="CommandRuns" Where="Client.Name == @Client">
            <WhereParameters>
                <asp:QueryStringParameter Name="Client" QueryStringField="c" Type="Object" />
            </WhereParameters>
        </asp:LinqDataSource>
        <hr />
        <asp:TextBox runat="server" ID="tbCmd" CssClass="form-control" Width="200" />
        <asp:TextBox runat="server" TextMode="DateTime" ID="tbAt" CssClass="form-control" Width="200" />
        <asp:Button runat="server" CssClass="btn btn-default" Text="Schedule" ID="BtnSchedule" OnClick="BtnSchedule_Click" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="tbCmd" CssClass="text-danger" ErrorMessage="The command field can't be empty." Display="None" />
        <asp:RequiredFieldValidator runat="server" ControlToValidate="tbAt" CssClass="text-danger" ErrorMessage="The schedule date field can't be empty." Display="None" />
        <p><asp:CustomValidator runat="server" Text="Invalid date-time field format" ControlToValidate="tbAt" OnServerValidate="Unnamed_ServerValidate" CssClass="text-danger" /></p>
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
    </div>
</asp:Content>
