﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLib
{
    [ServiceContract]
    public interface ISchedulerService
    {
        [OperationContract]
        void ScheduleExecution(string workstation, string commandLine, DateTime at);

        [OperationContract]
        void CancelSchedule(Guid id);

        [OperationContract]
        void RunExecution(Guid id);

        [OperationContract]
        IEnumerable<ScheduledExecution> GetSchedule();
    }
}
