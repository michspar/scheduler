﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLib.Data
{
    public class CommandRunContext : DbContext
    {
        public CommandRunContext()
            : base("DefaultConnection") { }

        public DbSet<Client> Clients { get; set; }
        public DbSet<ClientCommandRun> CommandRuns { get; set; }
        public DbSet<ClientLog> Logs { get; set; }
        public DbSet<Command> Commands { get; set; }

        public static CommandRunContext Create()
        {
            return new CommandRunContext();
        }

        T UpdateState<T>(T entity) where T: BaseEntity
        {
            Entry(entity).State = entity.Id == 0 ? EntityState.Added : EntityState.Unchanged;

            return entity;
        }

        public Client GetOrAddClient(string workstation)
        {
            return UpdateState(Clients.FirstOrDefault((x) => string.Compare(x.Name, workstation, true) == 0) ?? Clients.Add(new Client() { Name = workstation }));
        }

        public Command GetOrAddCommand(string command)
        {
            return UpdateState(Commands.FirstOrDefault((x) => string.Compare(x.AppWithArgs, command, true) == 0) ??
                Commands.Add(new Command() { AppWithArgs = command }));
        }

        public ClientCommandRun AddCommandRun(Client client, Command command, DateTime at)
        {
            return CommandRuns.Add(new ClientCommandRun()
            {
                ScheduleDate = at,
                Client = client,
                Command = command,
                Log = new ClientLog()
            });
        }
    }
}
