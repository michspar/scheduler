﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ClientLib
{
    [ServiceContract]
    public interface ICommandRunner
    {
        [OperationContract]
        CommandResult RunCommand(string commandLine);
    }
}
