﻿using SchedulerLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebUI.SchedulerServiceReference;
using Microsoft.AspNet.Identity.Owin;

namespace WebUI
{
    public partial class ManageClient : System.Web.UI.Page
    {
        SchedulerServiceClient client;
        CommandRunContext db;
        string clientName;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tbAt.Text = DateTime.Now.ToString();
            }

            db = Context.GetOwinContext().GetUserManager<CommandRunContext>();
            clientName = Request.QueryString["c"];
            Title = "Manage " + Request.QueryString["c"];
            client = new SchedulerServiceClient();
            ScheduledList.DataSource = client.GetSchedule().Where(sh => sh.Client == clientName).ToList();

            ScheduledList.DataBind();
        }

        protected void BtnSchedule_Click(object sender, EventArgs e)
        {
            DateTime at;
            if (string.IsNullOrWhiteSpace(tbCmd.Text) || string.IsNullOrWhiteSpace(tbAt.Text) || !DateTime.TryParse(tbAt.Text, out at))
                return;

            client.ScheduleExecution(clientName, tbCmd.Text, at);
            Response.Redirect(Request.Url.ToString());
        }

        protected void DeleteSchedule_Click(object sender, EventArgs e)
        {
            client.CancelSchedule(Guid.Parse((sender as LinkButton).CommandArgument));
            Response.Redirect(Request.Url.ToString());
        }

        protected void Exec_Click(object sender, EventArgs e)
        {
            client.RunExecution(Guid.Parse((sender as LinkButton).CommandArgument));
            Response.Redirect(Request.Url.ToString());
        }

        protected void Unnamed_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime tmp;

            args.IsValid = DateTime.TryParse(args.Value, out tmp);
        }
    }
}