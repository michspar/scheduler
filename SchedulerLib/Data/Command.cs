﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchedulerLib.Data
{
    public class Command : BaseEntity
    {
        public string AppWithArgs { get; set; }

        public virtual List<ClientCommandRun> Runs { get; set; }
    }
}
